const VSCodeInfo = require('./packages/vscode-build/vscode_version.json');

/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: './jest.domenvironment.js',
  testMatch: ['**/*.test.ts'],
  testPathIgnorePatterns: ['dist/', 'lib/', 'tmp/', 'gitlab-vscode-extension'],
  modulePathIgnorePatterns: ['dist/', 'gitlab-vscode-extension'],
  resetMocks: true,
  transformIgnorePatterns: ['node_modules/(?!(nanoid)/)'],
  transform: {
    '^.+\\.(js|ts)$': 'ts-jest',
  },
  globals: {
    VSCodeInfo,
    'ts-jest': {
      tsconfig: 'tsconfig.jest.json',
    },
  },
};
