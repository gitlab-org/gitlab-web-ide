import type { CrossWindowChannel } from '@gitlab/cross-origin-channel';
import type { AuthProvider } from '@gitlab/gitlab-api-client';
import { DefaultAuthProvider } from '@gitlab/gitlab-api-client';
import { asOAuthProvider, createOAuthClient, setupAutoRefresh } from '@gitlab/oauth-client';
import type { WebIdeConfig } from '@gitlab/web-ide-types';
import { PortChannelAuthProvider } from './PortChannelAuthProvider';

interface GetAuthProviderOptions {
  config: WebIdeConfig;
  windowChannel?: CrossWindowChannel;
  onTokenChange?: () => void;
}

export const getAuthProvider = async ({
  config,
  windowChannel,
  onTokenChange,
}: GetAuthProviderOptions): Promise<AuthProvider | undefined> => {
  if (config.auth?.type === 'token') {
    return new DefaultAuthProvider(config.auth.token);
  }

  if (config.auth?.type === 'oauth') {
    if (windowChannel) {
      const portChannel = await windowChannel.requestRemotePortChannel('auth-port');

      return new PortChannelAuthProvider({
        portChannel,
        onTokenChange,
      });
    }

    // TODO: Delete createOAuthClient along with the dedicatedWebIDEOrigin feature flag
    const client = createOAuthClient({
      oauthConfig: config.auth,
      gitlabUrl: config.gitlabUrl,
      owner: config.username,
    });

    setupAutoRefresh(client);

    if (onTokenChange) {
      client.onTokenChange(onTokenChange);
    }

    return asOAuthProvider(client);
  }

  return undefined;
};
