# GitLab Theme for Visual Studio Code

## Overview

The GitLab Theme for Visual Studio Code brings the sleek and professional look of GitLab to your coding environment. This theme is designed to enhance readability and reduce eye strain during long coding sessions, while maintaining the familiar GitLab aesthetic.

## Features

- Carefully crafted color palette inspired by GitLab's design.
- Optimized for long coding sessions.
- Consistent styling across the entire VS Code interface.

## Activating the Theme

1. Open the Command Palette (Ctrl+Shift+P or Cmd+Shift+P on macOS)
1. Type "Color Theme" and select "Preferences: Color Theme"
1. Choose any of the following themes from the list:
   1. GitLab Light.
   1. GitLab Dark.
   1. GitLab Dark Midnight.

## License

This theme is released under the MIT License.

## About GitLab

GitLab is a complete DevOps platform, delivered as a single application. From project planning and source code management to CI/CD, monitoring, and security.
