export enum FeatureFlags {
  // crossOriginExtensionHost -
  // This feature flag determines whether or not we use a separate origin
  // for the Web IDE extension host. Ideally, this would always be enabled,
  // but this breaks air-gapped instances that cannot reach out to the
  // internet. This should be resolved with the delivery of self-managed
  // cross-origin hosting of the Web IDE.
  // See https://gitlab.com/gitlab-org/gitlab-web-ide/-/merge_requests/404#note_2298883637
  CrossOriginExtensionHost = 'crossOriginExtensionHost',

  // projectPushRules -
  // This WIP feature flag enables the project push rule detection feature for
  // commit message linting based on the project's configured push rules.
  // See https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/388
  ProjectPushRules = 'projectPushRules',

  // languageServerWebIDE -
  // This WIP feature flag controls the availability of the Language Server used
  // in the GitLab Workflow VSCode Extension.
  // See https://gitlab.com/groups/gitlab-org/-/epics/14203
  LanguageServerWebIDE = 'languageServerWebIDE',

  // dedicatedWebIDEOrigin -
  // This WIP feature flag controls the availability of the feature to move the
  // Web IDE to be fully hosted on a separate origin.
  // See https://gitlab.com/groups/gitlab-org/-/epics/11972
  DedicatedWebIDEOrigin = 'dedicatedWebIDEOrigin',
}
