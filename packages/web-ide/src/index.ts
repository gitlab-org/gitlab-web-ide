import type { CrossWindowChannel, WindowChannelMessage } from '@gitlab/cross-origin-channel';
import type { WebIdeConfig, WebIde, OAuthCallbackConfig, Disposable } from '@gitlab/web-ide-types';
import { DefaultCrossWindowChannel } from '@gitlab/cross-origin-channel';
import { createOAuthClient } from '@gitlab/oauth-client';
import { checkOAuthToken } from './checkOAuthToken';
import type { UnloadPreventer } from './unloadPreventer';
import { createUnloadPreventer } from './unloadPreventer';
import { AuthPortChannelController } from './AuthPortChannelController';
import { handleSetHrefMessage } from './handleSetHrefMessage';
import { handleOpenUriMessage } from './handleOpenUriMessage';

export const createError = (msg: string) => new Error(`[gitlab-vscode] ${msg}`);

interface HandleMessagesOptions {
  windowChannel: CrossWindowChannel;
  config: WebIdeConfig;
  unloadPreventer: UnloadPreventer;
}

const handleMessages = ({ windowChannel, config, unloadPreventer }: HandleMessagesOptions) =>
  windowChannel.addMessagesListener((e: WindowChannelMessage) => {
    switch (e.key) {
      case 'error':
        config.handleError?.(e.params.errorType);
        break;
      case 'web-ide-tracking':
        config.handleTracking?.(e.params.event);
        break;
      case 'prevent-unload':
        unloadPreventer.setShouldPrevent(e.params.shouldPrevent);
        break;
      case 'update-web-ide-context':
        config.handleContextUpdate?.(e.params);
        break;
      case 'open-uri':
        handleOpenUriMessage(config, e);
        break;
      case 'set-href':
        handleSetHrefMessage(e);
        break;
      default:
        break;
    }
  });

const bootstrapWorkbench = async (windowChannel: CrossWindowChannel, config: WebIdeConfig) => {
  try {
    await windowChannel.waitForMessage('web-ide-config-request');

    windowChannel.postMessage({
      key: 'web-ide-config-response',
      params: {
        config: JSON.stringify(config),
      },
    });

    await windowChannel.waitForMessage('ready');

    /**
     * We interpret further Web IDE config requests as the result
     * of the iframe window that hosts the Web IDE being reloaded.
     * We want to react to this event by reloading the main window as well.
     */
    windowChannel.addMessageListener('web-ide-config-request', () => {
      window.location.reload();
    });
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    throw new Error('Could not initialize Web IDE');
  }
};

const startAnyConfig = (el: Element, config: WebIdeConfig): WebIde => {
  const unloadPreventer = createUnloadPreventer();
  const iframe = document.createElement('iframe');

  Object.assign(iframe.style, {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: '100%',
    border: 'none',
    margin: 0,
    padding: 0,
  });
  el.appendChild(iframe);

  if (!iframe.contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  const windowChannel = new DefaultCrossWindowChannel({
    localWindow: window,
    remoteWindow: iframe.contentWindow,
    remoteWindowOrigin: new URL(config.workbenchBaseUrl).origin,
  });

  const disposables: Array<Disposable> = [
    windowChannel,
    unloadPreventer,
    handleMessages({ windowChannel, config, unloadPreventer }),
  ];

  if (config.featureFlags?.dedicatedWebIDEOrigin && config.auth?.type === 'oauth') {
    const oauthClient = createOAuthClient({
      oauthConfig: config.auth,
      gitlabUrl: config.gitlabUrl,
      owner: config.username,
    });

    const authPortController = new AuthPortChannelController({
      oauthClient,
      authPort: windowChannel.createLocalPortChannel('auth-port'),
    });

    disposables.push(authPortController);
  }

  iframe.src = `${config.workbenchBaseUrl}/assets/workbench.html`;

  const ready = bootstrapWorkbench(windowChannel, config);

  return {
    dispose() {
      iframe.remove();
      disposables.forEach(disposable => disposable.dispose());
    },
    ready,
  };
};

export const start = async (el: Element, configArg: WebIdeConfig): Promise<WebIde> => {
  const config: WebIdeConfig = {
    ...configArg,
    /**
     * TODO: Remove this deprecated `extensionsGallerySettings` support once
     * the Rails app has been updated.
     * https://gitlab.com/gitlab-org/gitlab/-/issues/512642
     */
    extensionMarketplaceSettings:
      configArg.extensionMarketplaceSettings || configArg.extensionsGallerySettings,
    /**
     * TODO: Remove these fallback values once the embedderOriginUrl,
     * workbenchBaseUrl, and the extensionsHostBaseUrl are provided
     * by the GitLab Rails application.
     */
    embedderOriginUrl: configArg.embedderOriginUrl || configArg.baseUrl || '',
    workbenchBaseUrl: configArg.workbenchBaseUrl || configArg.baseUrl || '',
    extensionsHostBaseUrl:
      configArg.extensionsHostBaseUrl ||
      'https://{{uuid}}.cdn.web-ide.gitlab-static.net/web-ide-vscode/{{quality}}/{{commit}}',
  };

  await checkOAuthToken(config);

  return startAnyConfig(el, config);
};

export const oauthCallback = async (config: OAuthCallbackConfig) => {
  if (config.auth?.type !== 'oauth') {
    throw new Error('Expected config.auth to be OAuth config.');
  }

  const oauthClient = createOAuthClient({
    oauthConfig: config.auth,
    gitlabUrl: config.gitlabUrl,
    owner: config.username,
  });

  return oauthClient.handleCallback();
};
