export const stringToBuffer = (str: string): Buffer => Buffer.from(str, 'utf-8');
