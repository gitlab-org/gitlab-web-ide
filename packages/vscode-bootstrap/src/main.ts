// initialized by loading SCRIPT_VSCODE_LOADER 'vscode/out/vs/loader.js'
import type { WebIdeConfig, SerializableConfig } from '@gitlab/web-ide-types';
import type { WebIDEConfigResponseMessage } from '@gitlab/cross-origin-channel';
import { DefaultCrossWindowChannel } from '@gitlab/cross-origin-channel';
import { start } from './start';
import { insertScript } from './utils/insertScript';
import { insertMeta } from './utils/insertMeta';
import { getRepoRoot } from './utils/getRepoRoot';
import { loadGitLabFonts } from './utils/loadGitLabFonts';

const SCRIPT_VSCODE_LOADER = 'vscode/out/vs/loader.js';
const SCRIPT_VSCODE_WEB_PACKAGE_PATHS = 'vscode/out/vs/webPackagePaths.js';
const SCRIPT_VSCODE_WORKBENCH_NLS = 'vscode/out/nls.messages.js';
const SCRIPT_VSCODE_WORKBENCH = 'vscode/out/vs/workbench/workbench.web.main.js';

declare global {
  interface Window {
    // initialized by loading SCRIPT_VSCODE_LOADER
    trustedTypes: {
      createPolicy(...args: unknown[]): unknown;
    };

    // initialized by loading SCRIPT_VSCODE_WEB_PACKAGE_PATHS
    webPackagePaths: Record<string, string>;
  }
}

const getExtensionConfig = async (config: WebIdeConfig, extensionPath: string) => {
  const extensionPackageJSONUrl = `${config.workbenchBaseUrl}/vscode/extensions/${extensionPath}/package.json`;
  const rawJson = await fetch(extensionPackageJSONUrl).then(x => x.text());
  const packageJSON = JSON.parse(rawJson);

  return {
    extensionPath,
    packageJSON,
  };
};

const getBuiltInExtensions = async (config: SerializableConfig) =>
  Promise.all([
    getExtensionConfig(config, 'gitlab-web-ide'),
    getExtensionConfig(config, 'gitlab-language-support-vue'),
    getExtensionConfig(config, 'gitlab-vscode-extension'),
    getExtensionConfig(config, 'gitlab-vscode-theme'),
  ]);

/**
 * This makes sure that the navigator keyboard is compatible
 *
 * VSCode reads from this global sometimes and was throwing some errors... This might not be needed...
 */
const setupNavigatorKeyboard = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  if ((navigator as any).keyboard) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Object.assign((navigator as any).keyboard, {
      getLayoutMap: () => Promise.resolve(new Map()),
    });
  }
};

const setupAMDRequire = (config: WebIdeConfig) => {
  const vscodeUrl = `${config.workbenchBaseUrl}/vscode`;

  // eslint-disable-next-line no-restricted-globals, array-callback-return, prefer-arrow-callback, func-names
  Object.keys(self.webPackagePaths).map(function (key) {
    // eslint-disable-next-line no-restricted-globals
    self.webPackagePaths[
      key
      // eslint-disable-next-line no-restricted-globals
    ] = `${vscodeUrl}/node_modules/${key}/${self.webPackagePaths[key]}`;
  });

  require.config({
    baseUrl: `${vscodeUrl}/out`,
    recordStats: true,
    trustedTypesPolicy: window.trustedTypes?.createPolicy('amdLoader', {
      createScriptURL(value: string) {
        if (value.startsWith(vscodeUrl)) {
          return value;
        }
        throw new Error(`Invalid script url: ${value}`);
      },
    }),
    // eslint-disable-next-line no-restricted-globals
    paths: self.webPackagePaths,
  });
};

const main = async () => {
  const windowChannel = new DefaultCrossWindowChannel({
    localWindow: window,
    remoteWindow: window.parent,
    remoteWindowOrigin: '*',
  });

  windowChannel.postMessage({ key: 'web-ide-config-request' });

  const initWorkbenchMessage =
    await windowChannel.waitForMessage<WebIDEConfigResponseMessage>('web-ide-config-response');
  const config: WebIdeConfig = JSON.parse(initWorkbenchMessage.params.config);

  loadGitLabFonts(config.editorFont?.fontFaces);

  const extensionList = await getBuiltInExtensions(config);

  insertMeta('gitlab-builtin-vscode-extensions', extensionList);

  setupNavigatorKeyboard();

  await Promise.all([
    insertScript(`${config.workbenchBaseUrl}/${SCRIPT_VSCODE_LOADER}`, config.nonce),
    insertScript(`${config.workbenchBaseUrl}/${SCRIPT_VSCODE_WEB_PACKAGE_PATHS}`, config.nonce),
  ]);

  setupAMDRequire(config);

  await Promise.all([
    insertScript(`${config.workbenchBaseUrl}/${SCRIPT_VSCODE_WORKBENCH_NLS}`, config.nonce),
    insertScript(`${config.workbenchBaseUrl}/${SCRIPT_VSCODE_WORKBENCH}`, config.nonce),
  ]);

  if (config) {
    start({
      ...config,
      repoRoot: getRepoRoot(config.projectPath),
    });
  } else {
    // This shouldn't happen.
    throw new Error(`Unexpected config (${config}) when trying to start VSCode.`);
  }
};

main().catch(e => {
  // TODO: Lets do something nicer...
  // eslint-disable-next-line no-console
  console.error(e);
  // eslint-disable-next-line no-alert
  alert('An unexpected error occurred! Please open the developer console for details.');
});
