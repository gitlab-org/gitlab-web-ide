export const DEFAULT_SESSION_ID = 'current-user';
export const DEFAULT_DOCUMENTATION_URL = 'https://docs.gitlab.com/ee/user/web_ide';
