import type { FileSystem } from '@gitlab/web-ide-fs';
import * as vscode from 'vscode';
import { FS_SCHEME } from './constants';
import { GitLabFileSystemProvider } from './vscode/GitLabFileSystemProvider';
import { InitialFileSystemProvider } from './vscode/InitialFileSystemProvider';

let placeholderFileSystem: vscode.Disposable | undefined;

export function setupPlaceholderFileSystemProvider(repoRoot: string) {
  placeholderFileSystem = vscode.workspace.registerFileSystemProvider(
    FS_SCHEME,
    new InitialFileSystemProvider(repoRoot),
    {
      isReadonly: true,
    },
  );
}

export function teardownPlaceholderFileSystem() {
  if (placeholderFileSystem) {
    placeholderFileSystem.dispose();
    placeholderFileSystem = undefined;
  }
}

export function setupFileSystemProvider(
  disposables: vscode.Disposable[],
  fs: FileSystem,
  isReadonly: boolean,
) {
  teardownPlaceholderFileSystem();

  const vscodeFs = new GitLabFileSystemProvider(fs);

  disposables.push(
    vscode.workspace.registerFileSystemProvider(FS_SCHEME, vscodeFs, {
      isCaseSensitive: true,
      isReadonly,
    }),
  );
}
