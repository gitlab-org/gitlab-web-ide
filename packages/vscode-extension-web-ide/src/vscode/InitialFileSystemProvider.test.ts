import * as vscode from 'vscode';
import { FS_SCHEME } from '../constants';
import { InitialFileSystemProvider, INIT_FILE_STAT } from './InitialFileSystemProvider';

const MOCK_REPO_ROOT = 'gitlab-test';

const createMockFsUri = (path: string) => vscode.Uri.from({ scheme: FS_SCHEME, path });

describe('vscode/InitialFileSystemProvider', () => {
  let initialFileSystemProvider: InitialFileSystemProvider;

  beforeEach(() => {
    initialFileSystemProvider = new InitialFileSystemProvider(MOCK_REPO_ROOT);
  });

  describe('stat', () => {
    it('returns noop file stats if uri passed is repo root', async () => {
      const MOCK_URI = createMockFsUri(MOCK_REPO_ROOT);
      expect(initialFileSystemProvider.stat(MOCK_URI)).toEqual(INIT_FILE_STAT);
    });

    it('throws error if uri passed is not repo root', async () => {
      const MOCK_URI = createMockFsUri('gitlab-not-repo');
      expect(() => initialFileSystemProvider.stat(MOCK_URI)).toThrowError(
        vscode.FileSystemError.FileNotFound(MOCK_URI),
      );
    });
  });
});
