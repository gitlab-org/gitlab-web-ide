import * as vscode from 'vscode';

export const NOOP_DISPOSABLE = vscode.Disposable.from();
