import type { WebIdeExtensionConfig } from '@gitlab/web-ide-types';
import * as vscode from 'vscode';

import { FS_SCHEME } from '../constants';
import { getConfig } from '../mediator';
import { requestSchemaContent } from './redhatVscodeYaml';

jest.mock('../mediator');

const MOCK_REPO_ROOT = 'root';
const MOCK_URI_STR = `${FS_SCHEME}://~/path/my-schema.json`;

describe('requestSchemaContent', () => {
  beforeEach(() => {
    jest.mocked(getConfig).mockResolvedValue({ repoRoot: MOCK_REPO_ROOT } as WebIdeExtensionConfig);
  });

  it('appends repo root to path', async () => {
    await requestSchemaContent(MOCK_URI_STR);
    expect(jest.mocked(vscode.workspace.fs.readFile)).toBeCalledWith(
      expect.objectContaining({ path: `/${MOCK_REPO_ROOT}/path/my-schema.json` }),
    );
  });

  it('shows error message if file cannot be read', async () => {
    jest.mocked(vscode.workspace.fs.readFile).mockRejectedValueOnce(new Error('File not found'));
    await requestSchemaContent(MOCK_URI_STR);
    await expect(jest.mocked(vscode.window.showErrorMessage)).toBeCalledWith(
      `Cannot read YAML schema: ${MOCK_URI_STR}`,
    );
  });

  it('removes `~` from path if it exists', async () => {
    const mockBadUriStr = `gitlab-web-ide:/~/path/to/file.json`;
    await requestSchemaContent(mockBadUriStr);
    expect(jest.mocked(vscode.workspace.fs.readFile)).toBeCalledWith(
      expect.objectContaining({ path: `/${MOCK_REPO_ROOT}/path/to/file.json` }),
    );
  });
});
