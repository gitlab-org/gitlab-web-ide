import { joinPaths } from '@gitlab/utils-path';
import * as vscode from 'vscode';
import { FS_SCHEME } from '../constants';
import { getConfig } from '../mediator';
import { log } from '../utils/log';

export const EXTENSION_ID = 'redhat.vscode-yaml';

// source: https://github.com/redhat-developer/vscode-yaml/blob/main/src/schema-extension-api.ts#L48
export interface VscodeYamlExtensionApi {
  registerContributor?: (
    schema: string,
    requestSchema: (resource: string) => string,
    requestSchemaContent: (uri: string) => Promise<string>,
    label?: string,
  ) => boolean;
}

function requestSchema(): string {
  return '';
}

// Expected `uriStr` format is `gitlab-web-ide://~/path/to/file.json`
export async function requestSchemaContent(uriStr: string): Promise<string> {
  const { repoRoot } = await getConfig();

  const uriArg = vscode.Uri.parse(uriStr);

  // Somehow, when a remote schema is present the `uriStr` passed is translated to `gitlab-web-ide:/~/path/to/file.json`
  // This leads to incorrect parsing of the Uri (`~` is included as a path) causing it to fail when attempting to read the file.
  // Let's remove this `~` if it exists and is in the beginning of the path.
  const pathNoTilde = uriArg.path.replace(/^\/~/, '');

  const uri = vscode.Uri.from({
    scheme: uriArg.scheme,
    path: joinPaths('/', repoRoot, pathNoTilde),
  });

  let content: Uint8Array;
  try {
    content = await vscode.workspace.fs.readFile(uri);
  } catch (e) {
    log.error(`Error reading YAML schema for ${uriStr}`, e);
    await vscode.window.showErrorMessage(`Cannot read YAML schema: ${uriStr}`);
    return '';
  }

  const decoder = new TextDecoder('utf-8');
  const result = decoder.decode(content);
  return result;
}

export async function setup(extension?: vscode.Extension<VscodeYamlExtensionApi>) {
  const api = await extension?.activate();

  if (!api) {
    return;
  }

  if (!api.registerContributor) {
    log.info(
      'Attempt to register schema in vscode-yaml extension failed. registerContributor method is not defined',
    );
    return;
  }

  api.registerContributor(FS_SCHEME, requestSchema, requestSchemaContent);
}
