import * as vscode from 'vscode';
import type { FileSystem } from '@gitlab/web-ide-fs';
import { createFileSystemMock } from '../test-utils/fs';
import { FS_SCHEME } from './constants';
import {
  teardownPlaceholderFileSystem,
  setupPlaceholderFileSystemProvider,
  setupFileSystemProvider,
} from './setupFileSystemProvider';
import { GitLabFileSystemProvider } from './vscode/GitLabFileSystemProvider';
import { InitialFileSystemProvider } from './vscode/InitialFileSystemProvider';

const MOCK_PLACEHOLDER_FS_PROVIDER_OPTIONS = {
  isReadonly: true,
};

const MOCK_FS_PROVIDER_OPTIONS = {
  isCaseSensitive: true,
  isReadonly: true,
};

const MOCK_REPO_ROOT = 'gitlab-test';

describe('setupFileSystemProvider', () => {
  beforeEach(() => {
    jest.mocked(vscode.workspace.registerFileSystemProvider).mockImplementation(() => ({
      dispose: jest.fn(),
    }));
  });

  afterEach(() => {
    teardownPlaceholderFileSystem();
  });

  const getRegistrations = () =>
    jest
      .mocked(vscode.workspace.registerFileSystemProvider)
      .mock.results.reduce<
        vscode.Disposable[]
      >((acc, { type, value }) => (type === 'return' ? acc.concat(value) : acc), []);

  describe('setupPlaceholderFileSystemProvider', () => {
    it('registers noop file system', () => {
      setupPlaceholderFileSystemProvider(MOCK_REPO_ROOT);

      expect(vscode.workspace.registerFileSystemProvider).toBeCalledTimes(1);
      expect(vscode.workspace.registerFileSystemProvider).toBeCalledWith(
        FS_SCHEME,
        new InitialFileSystemProvider(MOCK_REPO_ROOT),
        MOCK_PLACEHOLDER_FS_PROVIDER_OPTIONS,
      );
    });
  });

  describe('setupFileSystemProvider', () => {
    let mockDisposables: vscode.Disposable[];
    let mockFs: FileSystem;

    beforeEach(() => {
      mockDisposables = [];
      mockFs = createFileSystemMock();
    });

    it('registers file system', () => {
      setupFileSystemProvider(mockDisposables, mockFs, MOCK_FS_PROVIDER_OPTIONS.isReadonly);

      expect(vscode.workspace.registerFileSystemProvider).toBeCalledTimes(1);
      expect(vscode.workspace.registerFileSystemProvider).toBeCalledWith(
        FS_SCHEME,
        new GitLabFileSystemProvider(mockFs),
        MOCK_FS_PROVIDER_OPTIONS,
      );
    });

    it('adds file system registration to given disposables', () => {
      setupFileSystemProvider(mockDisposables, mockFs, MOCK_FS_PROVIDER_OPTIONS.isReadonly);

      expect(mockDisposables).toEqual(getRegistrations());
    });

    it('tears down placeholder file system before registering if it exists', () => {
      setupPlaceholderFileSystemProvider(MOCK_REPO_ROOT);
      setupFileSystemProvider(mockDisposables, mockFs, MOCK_FS_PROVIDER_OPTIONS.isReadonly);

      const disposables = getRegistrations();
      expect(vscode.workspace.registerFileSystemProvider).toHaveBeenCalledTimes(2);
      expect(disposables[0].dispose).toHaveBeenCalled();
      expect(disposables[1].dispose).not.toHaveBeenCalled();
      expect(vscode.workspace.registerFileSystemProvider).toBeCalledWith(
        FS_SCHEME,
        new GitLabFileSystemProvider(mockFs),
        MOCK_FS_PROVIDER_OPTIONS,
      );
    });
  });
});
