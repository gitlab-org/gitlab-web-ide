import * as vscode from 'vscode';
import { COMMAND_COMMIT } from '@gitlab/vscode-mediator-commands';
import type { FileStatus } from '@gitlab/web-ide-fs';
import { FileStatusType } from '@gitlab/web-ide-fs';
import { NOOP_AUTH_PROVIDER } from '@gitlab/gitlab-api-client';
import type { CommitCommand, ReadonlySourceControlViewModel } from '../types';
import type { LocalStorage } from '../../types';
import { generateCommitMessage } from './generateCommitMessage';
import { getBranchSelection } from './getBranchSelection';
import { factory } from './command';
import { getCommitPayload } from './getCommitPayload';
import { RELOAD_COMMAND_ID } from '../../constants';
import { TEST_PROJECT, TEST_REF_BRANCH } from '../../../test-utils';
import { createFakeGlobalState } from '../../../test-utils/vscode';
import { showSuccessMessage } from './showSuccessMessage';
import { showCommitErrorMessage } from './showCommitErrorMessage';
import DefaultLocalStorage from '../../DefaultLocalStorage';
import { setupFakeMediatorToken } from '../../../test-utils/setupFakeMediatorToken';
import { setupMediatorCommandExecutor } from '../../mediator';

jest.mock('./getBranchSelection');
jest.mock('./generateCommitMessage');
jest.mock('./showSuccessMessage');
jest.mock('./showCommitErrorMessage');

const TEST_COMMIT_MESSAGE = 'Hello world! Test commit!';
const TEST_GENERATED_COMMIT_MESSAGE = 'Genearted commit message:\n\n123';
const TEST_BRANCH_MR_URL = 'https://gitlab.example.com/mr/1';
const TEST_BRANCH_SELECTION = {
  branchName: 'foo-branch-patch-123',
  isNewBranch: false,
};
const TEST_COMMIT_ID = '000000111111';
const TEST_STATUS: FileStatus[] = [{ type: FileStatusType.Deleted, path: '/README.md' }];
const TEST_MEDIATOR_TOKEN = 'fake-mediator-token';

describe('scm/commit/command', () => {
  let viewModel: jest.MockedObject<ReadonlySourceControlViewModel>;
  let command: CommitCommand;
  let localStorage: LocalStorage;

  beforeEach(async () => {
    localStorage = new DefaultLocalStorage(createFakeGlobalState());
    viewModel = {
      getCommitMessage: jest.fn().mockReturnValue(TEST_COMMIT_MESSAGE),
      getStatus: jest.fn().mockReturnValue(TEST_STATUS),
    };
    jest.mocked(getBranchSelection).mockResolvedValueOnce(TEST_BRANCH_SELECTION);
    jest.mocked(generateCommitMessage).mockReturnValue(TEST_GENERATED_COMMIT_MESSAGE);

    setupFakeMediatorToken(TEST_MEDIATOR_TOKEN);
    await setupMediatorCommandExecutor(NOOP_AUTH_PROVIDER);
  });

  describe('with default dependencies', () => {
    beforeEach(() => {
      command = factory({
        project: TEST_PROJECT,
        ref: TEST_REF_BRANCH,
        branchMergeRequestUrl: TEST_BRANCH_MR_URL,
        viewModel,
        localStorage,
      });
    });

    describe('default', () => {
      beforeEach(async () => {
        await command();
      });

      it('calls commit mediator command', () => {
        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
          COMMAND_COMMIT,
          TEST_MEDIATOR_TOKEN,
          getCommitPayload({
            status: TEST_STATUS,
            commitMessage: TEST_COMMIT_MESSAGE,
            startingSha: TEST_COMMIT_ID,
            branchName: TEST_BRANCH_SELECTION.branchName,
            isNewBranch: false,
          }),
        );
      });

      it('shows success message', () => {
        expect(showSuccessMessage).toHaveBeenCalledWith({
          project: TEST_PROJECT,
          ref: TEST_REF_BRANCH,
          commitBranchName: TEST_BRANCH_SELECTION.branchName,
          mrUrl: TEST_BRANCH_MR_URL,
        });
      });

      it('calls reload command', () => {
        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(RELOAD_COMMAND_ID, {
          ref: TEST_BRANCH_SELECTION.branchName,
        });
      });

      it('calls getBranchSelection', () => {
        expect(getBranchSelection).toHaveBeenCalledTimes(1);
        expect(getBranchSelection).toHaveBeenCalledWith({
          project: TEST_PROJECT,
          ref: TEST_REF_BRANCH,
          localStorage,
          shouldPromptBranchName: false,
        });
      });
    });

    describe('when branch selection is undefined', () => {
      beforeEach(async () => {
        jest.mocked(getBranchSelection).mockReset().mockResolvedValueOnce(undefined);
        await command();
      });

      it('does not call commit mediator command', () => {
        expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
      });
    });

    describe('with new branch selection', () => {
      beforeEach(async () => {
        jest
          .mocked(getBranchSelection)
          .mockReset()
          .mockResolvedValueOnce({ ...TEST_BRANCH_SELECTION, isNewBranch: true });
        await command();
      });

      it('does not include branch MR URL in showSuccessMessage', () => {
        expect(showSuccessMessage).toHaveBeenCalledWith({
          project: TEST_PROJECT,
          commitBranchName: TEST_BRANCH_SELECTION.branchName,
          ref: TEST_REF_BRANCH,
          mrUrl: '',
        });
      });
    });

    describe('when commit fails', () => {
      const testError = new Error();

      beforeEach(async () => {
        jest.spyOn(console, 'error').mockImplementation();
        jest.mocked(vscode.commands.executeCommand).mockRejectedValue(testError);
        await command();
      });

      it('logs and shows error message', () => {
        // eslint-disable-next-line no-console
        expect(console.error).toHaveBeenCalledWith(testError);

        expect(showCommitErrorMessage).toHaveBeenCalledWith(testError);
      });

      it('does not call reload command', () => {
        expect(vscode.commands.executeCommand).toHaveBeenCalledTimes(1);
        expect(vscode.commands.executeCommand).not.toHaveBeenCalledWith(
          RELOAD_COMMAND_ID,
          expect.anything(),
        );
      });

      it('does not show success message', () => {
        expect(vscode.window.showInformationMessage).not.toHaveBeenCalled();
      });
    });

    describe('when viewModel.getCommitMessage is empty', () => {
      beforeEach(async () => {
        viewModel.getCommitMessage.mockReturnValue('');
        await command();
      });

      it('generates commit message for payload', () => {
        expect(generateCommitMessage).toHaveBeenCalledWith(TEST_STATUS);
        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
          COMMAND_COMMIT,
          TEST_MEDIATOR_TOKEN,
          expect.objectContaining({
            commit_message: TEST_GENERATED_COMMIT_MESSAGE,
          }),
        );
      });
    });

    describe('with empty status', () => {
      beforeEach(async () => {
        viewModel.getStatus.mockReturnValue([]);
        await command();
      });

      it('shows information message', () => {
        expect(vscode.window.showInformationMessage).toHaveBeenCalledWith(
          expect.stringMatching('No changes found'),
        );
      });

      it('does not execute any other commands', () => {
        expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
      });
    });

    describe('with shouldPromptBranchName options', () => {
      beforeEach(async () => {
        await command({ shouldPromptBranchName: true });
      });

      it('calls getBranchSelection with shouldPromptBranchName: true', () => {
        expect(getBranchSelection).toHaveBeenCalledTimes(1);
        expect(getBranchSelection).toHaveBeenCalledWith(
          expect.objectContaining({
            shouldPromptBranchName: true,
          }),
        );
      });
    });

    describe('with null options', () => {
      beforeEach(async () => {
        await command(null);
      });

      it('calls getBranchSelection with shouldPromptBranchName: false', () => {
        expect(getBranchSelection).toHaveBeenCalledTimes(1);
        expect(getBranchSelection).toHaveBeenCalledWith(
          expect.objectContaining({
            shouldPromptBranchName: false,
          }),
        );
      });
    });
  });
});
